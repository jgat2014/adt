<style type="text/css">
	#table_view {
		width: 40%;
	}
	#output {
		width: 600px;
		height: 200px;
		background: #FFF;
		overflow: scroll;
		font-family: Verdana;
	}
	#table_select {
		display: none;
		padding: 5px;
		width: 400px;
	}
	#table_list {
		width: 250px;
		zoom: 85%;
	}
</style>
<div class="full-content" id="table_view" style="background:#9CF;">
	<div>
		<ul class="breadcrumb">
			<li>
				<a href="<?php echo site_url().'home_controller/home' ?>"><i class="icon-home"></i><strong>Home</strong></a>
				<span class="divider">/</span>
			</li>
			<li class="active" id="actual_page"></li>
		</ul>
	</div>
	<?php echo form_open('backup_management/backup_db');?>
	<?php echo form_fieldset('webADT Database Backup');?>
	<?php echo $this -> session -> flashdata('error_message');?>
	<br/>
	<?php echo form_label('Backup Location', 'pc_user');?>
	<?php echo form_input(array('name' => 'pc_user', 'id' => 'pc_user', 'size' => '50', 'class' => 'textfield form-control', 'placeholder' => 'D:\Backups', 'required' => 'required'));?>
	<br/>
	<?php echo form_submit(array('name' => 'input_go', 'value' => 'Perform Backup', 'class' => 'btn green'));?>
	<?php echo form_fieldset_close();?>
	<?php echo form_close();?>
</div>
<script type="text/javascript">
	$(document).ready(function() {
             $("#actual_page").text("<?php echo $actual_page;?>");
	});
</script>
